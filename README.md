# **REAL HOLTER**

Development of an autonomous system capable of monitoring, in real-time, vital parameters like: body temperature, ECG, heart rate or respiratory rate; detecting critical situations and acting immediately by sending notifications (email and FCM) to the affected user. 
In addition, the system converts the raw data measured by the holter to a standard format, EDF+. The system has the following elements:

 - Wireless holter (owned by the company Cortrium, Denmark)
 - Android app (Background running, Google Sign-in, Sensor pairing)
 - Data Analytics Platform (WSO2 DAS)
 - Python script (Flask, FCM & EDF libraries)


----------


## Android Application

#### Version 1.0

>  - Google Sign-In authentication
>  - Sensor - App pairing
>  - Background running
>  - State notifications

![](images/Login page - Android.png)
![](images/Main page - Android.png)
![](images/Pairing page - Android.png)

----------

## Server

#### Version 1.0
> 
>  - Real-time fever alert
>  - FCM notifications
>  - Email notifications
>  - Data conversion to EDF+
>  - Logged users tracker
