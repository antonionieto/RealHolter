package com.tfg.realholter;

import android.app.Application;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.cortrium.opkit.ConnectionManager;
import com.cortrium.opkit.CortriumC3;
import com.cortrium.opkit.datapackages.EcgData;
import com.cortrium.opkit.datatypes.SensorMode;
import com.google.gson.JsonObject;
import com.tfg.realholter.activity.MainActivity;
import com.tfg.realholter.model.Constants;
import com.tfg.realholter.model.Extra;

import java.util.HashMap;
import java.util.Map;


/**
 * Service that manages the monitor service. It runs in background and send the
 * data to the server.
 *
 * @author Antonio Javier Nieto García
 * @version September 2017
 */
public class RealHolterService extends Service implements ConnectionManager.OnConnectionManagerListener, ConnectionManager.EcgDataListener {

    //Service Binder
    private final IBinder mBinder = new LocalBinder();

    // Sensor connection parameters
    private ConnectionManager connectionManager;
    private CortriumC3 cortriumSensor;
    private boolean service_status;
    private String paired_sensor;

    // Google account user ID
    private String user_id;

    // Sensor data conversion parameters
    private boolean gain_state;
    private double conversion_constant;

    // Banderas de notificaciones
    private boolean batteryFlag;

    private BluetoothAdapter btadapter;
    private SharedPreferences preferences;



    public RealHolterService() {

    }

    @Override
    public void onCreate() {

        super.onCreate();

        showNotification(getString(R.string.notification_started));

        // Initializing sensor data conversion parameters
        gain_state = false;
        conversion_constant = 0;

        // Initializing notifications flags
        batteryFlag = false;

        // Gets sensor connection parameters
        preferences = getSharedPreferences(Application.class.getName(), MODE_PRIVATE);
        paired_sensor = preferences.getString(Extra.PAIRED_SENSOR, "Null");
        service_status = false;

        // Gets user ID from the system memory
        user_id = preferences.getString(Extra.USER_ID, "null");

        btadapter = BluetoothAdapter.getDefaultAdapter();

        // Initializing connection parameters
        connectionManager = null;
        connectionManager = ConnectionManager.getInstance(this);
        connectionManager.setConnectionManagerListener(this);
        connectionManager.setEcgDataListener(this);

    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        showNotification("RealHolter service started!");

        // Updates the service status variable
        service_status = true;

        // Enables bluetooth connection when it is disabled
        if (!btadapter.isEnabled())
            btadapter.enable();

        // Starts scanning when starting the monitor service
        connectionManager.startScanning();

        return super.onStartCommand(intent, flags, startId);

    }


    @Override
    public void onDestroy() {

        super.onDestroy();

        showNotification(getString(R.string.notification_stoped));

        // Updates the service status variable
        service_status = false;

        // Changes the sensor running mode
        if (cortriumSensor != null) {
            cortriumSensor.changeMode(CortriumC3.DeviceModes.DeviceModeIdle);
        }

        // Disconnects from the sensor and stops scanning
        connectionManager.disconnect();
        connectionManager.stopScanning();

    }


    @Override
    public IBinder onBind(Intent intent) {

        return mBinder;

    }



    public class LocalBinder extends Binder {

        RealHolterService getService() {

            // Return this instance of LocalService so clients can call public methods
            return RealHolterService.this;

        }

    }



    @Override
    public void discoveredDevice(CortriumC3 cortriumC3) {

        showNotification("Device discovered with ID: " + cortriumC3.getName());

        // Connects to the paired sensor
        if (cortriumC3.getName().equals(paired_sensor) && service_status){

            showNotification("Connecting to paired sensor...");

            connectionManager.connect(cortriumC3);

        }

    }


    @Override
    public void startedScanning(ConnectionManager connectionManager) {
    }


    @Override
    public void stoppedScanning(ConnectionManager connectionManager) {
    }


    @Override
    public void connectedToDevice(CortriumC3 cortriumC3) {

        showNotification("Connected to the device!");

        cortriumSensor = cortriumC3;
        connectionManager.stopScanning();

    }


    @Override
    public void disconnectedFromDevice(CortriumC3 cortriumC3) {

        showNotification("Disconnected from the device!");

        cortriumSensor = null;
        connectionManager.stopScanning();

    }


    @Override
    public void modeRead(SensorMode sensorMode) {
    }


    @Override
    public void deviceInformationRead(CortriumC3 cortriumC3) {
    }


    @Override
    public void ecgDataUpdated(final EcgData ecgData) {

        new Thread(new Runnable() {
            @Override
            public void run() {

                // Getting the gain from the device
                if (!gain_state) {

                    // Initializing the gain to the default value
                    int gain = 1;

                    switch (ecgData.getMiscInfo().getConfigurationBitmask() & 0xE0) {

                        case Constants.gains.CONF_GAIN_1:
                            Log.d("GAIN", "gain = 1");
                            gain = 1;
                            break;
                        case Constants.gains.CONF_GAIN_2:
                            Log.d("GAIN", "gain = 2");
                            gain = 2;
                            break;
                        case Constants.gains.CONF_GAIN_3:
                            Log.d("GAIN", "gain = 3");
                            gain = 3;
                            break;
                        case Constants.gains.CONF_GAIN_4:
                            Log.d("GAIN", "gain = 4");
                            gain = 4;
                            break;
                        case Constants.gains.CONF_GAIN_6:
                            Log.d("GAIN", "gain = 6");
                            gain = 6;
                            break;
                        case Constants.gains.CONF_GAIN_8:
                            Log.d("GAIN", "gain = 8");
                            gain = 8;
                            break;
                        case Constants.gains.CONF_GAIN_12:
                            Log.d("GAIN", "gain = 12");
                            gain = 12;
                            break;
                        default:
                            break;

                    }

                    // Calculates the conversion constant by the first time
                    conversion_constant = (Math.pow(2, 24) / 4800000) * gain;
                    gain_state = true;

                }


                // Checks if the connection is coorect
                if (ecgData.getMiscInfo().getLeadOffBits() == 0 && ecgData.getFilteredEcg1Samples() != null) {

                    showNotification("Good connection! - Battery level: " + ecgData.getMiscInfo().getBatteryLevel());

                    // Send low battery notification
                    if (!batteryFlag && ecgData.getMiscInfo().getBatteryLevel() < Constants.LOW_BATTERY_THRESHOLD) {

                        batteryFlag = true;
                        lowBatteryNotification();

                    } else if (batteryFlag && ecgData.getMiscInfo().getBatteryLevel() >= Constants.LOW_BATTERY_THRESHOLD) {

                        batteryFlag = false;
                        lowBatteryNotification();

                    }


                    // Gets the ECG data from the message received from the sensor
                    int[] ecg_original = ecgData.getFilteredEcg1Samples();

                    // Temperature data conversion
                    double temperature = ecgData.getMiscInfo().getTemperature() * 0.02 - 273.15;


                    // Converting the data using the conversion constant
                    double[] ecg_converted = new double[6];

                    for (int i = 0; i < 6; i++) {

                        ecg_converted[i] = (ecg_original[i] / conversion_constant);

                    }


                    // Gets the current timestamp to be sent to the server
                    Long timestamp = System.currentTimeMillis();
                    sendData(timestamp, ecg_converted, temperature, ecgData.getRrInterval(), ecgData.getRespiratoryRate(), ecgData.getHeartRate());

                } else {

                    showNotification(getString(R.string.notification_bad_connection));

                }

            }

        }).start();

    }


    /**
     * Send the sensor data to the server.
     *
     * @param timestamp
     * @param ecg_value
     * @param temperature
     * @param rr_interval
     * @param respiratory_rate
     * @param heart_rate
     */
    public void sendData(long timestamp, double[] ecg_value, double temperature, int rr_interval, int respiratory_rate, int heart_rate) {

        // Building message content
        String eventString = "[";
        for (int i=0; i<Constants.ECG_SAMPLE_SIZE; i++) {

            JsonObject event2 = new JsonObject();
            event2.addProperty(Constants.request.USER_ID, Float.valueOf(user_id.trim()));
            event2.addProperty(Constants.request.TIME, (timestamp + i*4));
            event2.addProperty(Constants.request.ECG_VALUE, ecg_value[i]);
            event2.addProperty(Constants.request.BODY_TEMPERATURE, temperature);
            event2.addProperty(Constants.request.RR_INTERVAL, rr_interval);
            event2.addProperty(Constants.request.RESPIRATORY_RATE, respiratory_rate);
            event2.addProperty(Constants.request.HEAR_RATE, heart_rate);

            eventString += event2.toString() + ", ";

        }
        eventString += "]";

        final String bodyString = eventString;

        // Sends the HTTPS message to the server
        StringRequest request = new StringRequest(Request.Method.POST, Constants.url.HEART_RATE_RECEIVER,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();

                // Sets the header parameters
                String credentials = Constants.AUTH_USER + ":" + Constants.AUTH_PASSWORD;
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);

                return headers;

            }

            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();

                return params;

            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                return bodyString.getBytes();

            }

        };

        // Adds the request to the application requests queue
        RealHolterApplication.getInstance().getRequestQueue().add(request);

    }


    /**
     * Shows an alert in the notifications bar.
     *
     * @param message
     */
    public void showNotification(String message)
    {

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.notification_title))
                .setContentText(message)
                .setSmallIcon(R.drawable.holter_notification);

        Intent notificacionIntent = new Intent(getApplicationContext(), MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(notificacionIntent);

        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setContentIntent(resultPendingIntent);
        notificationManager.notify(0, notification.build());

    }


    /**
     * Alerts the user when the battery is under a critical threshold
     */
    public void lowBatteryNotification () {

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        if (batteryFlag) {

            NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                    .setContentTitle(getString(R.string.notification_title))
                    .setContentText(getString(R.string.notification_low_battery))
                    .setSmallIcon(R.drawable.holter_notification)
                    .setVibrate(new long[] {100, 250, 100, 500});

            Intent notificacionIntent = new Intent(getApplicationContext(), MainActivity.class);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
            stackBuilder.addParentStack(MainActivity.class);

            stackBuilder.addNextIntent(notificacionIntent);

            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            notification.setContentIntent(resultPendingIntent);

            notificationManager.notify(2, notification.build());

        } else {

            notificationManager.cancel(2);

        }

    }


}
