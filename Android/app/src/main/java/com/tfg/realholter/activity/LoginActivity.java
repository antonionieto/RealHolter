package com.tfg.realholter.activity;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonObject;
import com.tfg.realholter.R;
import com.tfg.realholter.RealHolterApplication;
import com.tfg.realholter.model.Constants;
import com.tfg.realholter.model.Extra;

import java.util.HashMap;
import java.util.Map;


/**
 * This class allows user to log in the application using
 * Google Sign-In.
 *
 * @author Antonio Javier Nieto García
 * @version September 2017
 *
 */
public class LoginActivity extends Activity {

    // Google Sign-In request success code
    private static int RC_SIGN_IN = 9001;

    private GoogleApiClient mGoogleApiClient;
    private SharedPreferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        preferences = getSharedPreferences(Application.class.getName(), MODE_PRIVATE);

        // Gets GoogleApiClient instance and connect to Google Play services
        mGoogleApiClient = RealHolterApplication.getInstance().get_GoogleApiClient();
        mGoogleApiClient.connect();

        // Sets the dimensions of the sign-in button and the click listener
        SignInButton signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.sign_in_button:
                        login();
                        break;
                }
            }
        });

    }


    /**
     * Manages the "About" button located in the login screen
     *
     * @param view
     */
    public void onAboutClicked(View view) {

        Class<? extends Activity> activity = AboutActivity.class;
        Intent intent = new Intent(LoginActivity.this, activity);
        startActivity(intent);
    }


    /**
     * Gets the sign-in intent from Google Sign-In login.
     */
    private void login() {

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }


    /**
     * Gets the intent result from the login request and call the function
     * {@link #handleSignInResult(GoogleSignInResult)}.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }

    }

    /**
     * Handles the login request result from Google Sign-In.
     *
     * @param result
     */
    private void handleSignInResult(GoogleSignInResult result) {

        if (result.isSuccess()) {

            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            // Gets user information from the Google account
            String user_id = acct.getId().trim();
            String user_email = acct.getEmail();

            // Gets the application FCM token
            String user_token = FirebaseInstanceId.getInstance().getToken();

            // Saves the gathered information
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(Extra.USER_ID, user_id);
            editor.putString(Extra.USER_EMAIL, user_email);
            editor.putString(Extra.USER_TOKEN, user_token);
            editor.apply();

            sendLogin(user_id, user_email, user_token);

            // Shows the main page
            Class<? extends Activity> activity = MainActivity.class;
            Intent intent = new Intent(LoginActivity.this, activity);
            startActivity(intent);

        }
    }


    /**
     * Sends the user information to the server.
     *
     * @param id User's ID gathered from his Google account
     * @param email User's email gathered from his Google account
     * @param token FCM token of the application
     */
    public void sendLogin (String id, String email, String token) {

        // Forms the content of the HTTPS message
        String bodyString = "[";
        JsonObject login_data = new JsonObject();
        login_data.addProperty(Constants.request.USER_ID, id);
        login_data.addProperty(Constants.request.USER_EMAIL, email);
        login_data.addProperty(Constants.request.USER_TOKEN, token);
        bodyString += login_data.toString();
        bodyString += "]";

        final String content = bodyString;

        // Sends the HTTPS message to the server
        StringRequest request = new StringRequest(Request.Method.POST, Constants.url.LOGIN_RECEIVER,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {}

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {}

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();

                // Header parameters
                String credentials = Constants.AUTH_USER + ":" + Constants.AUTH_PASSWORD;
                String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);

                return headers;

            }

            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();

                return params;

            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                return content.getBytes();

            }
        };

        // Adds the request to the application requests queue
        RealHolterApplication.getInstance().getRequestQueue().add(request);

    }


}


