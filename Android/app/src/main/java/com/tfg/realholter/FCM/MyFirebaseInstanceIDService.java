package com.tfg.realholter.FCM;

import android.app.Application;
import android.content.SharedPreferences;
import android.util.Base64;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.gson.JsonObject;
import com.tfg.realholter.RealHolterApplication;
import com.tfg.realholter.model.Constants;
import com.tfg.realholter.model.Extra;

import java.util.HashMap;
import java.util.Map;


/**
 * Service that manages the FCM tokens.
 *
 * @author Antonio Javier Nieto García
 * @version September 2017
 *
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private SharedPreferences preferences;


    @Override
    public void onCreate() {

        preferences = getSharedPreferences(Application.class.getName(), MODE_PRIVATE);

    }


    @Override
    public void onTokenRefresh() {

        // Get the new token and refresh the new token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        // User parameters
        String user_id = preferences.getString(Extra.USER_ID, "null");
        String user_email = preferences.getString(Extra.USER_EMAIL, "null");

        if (user_id != "null") {

            // Saves the new token in the system memory
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(Extra.USER_TOKEN, refreshedToken);
            editor.apply();

            sendLogin(user_id, user_email, refreshedToken);

        }

    }


    /**
     * Sends to the server the new token and the user information related to
     * the application.
     *
     * @param id
     * @param email
     * @param token
     */
    public void sendLogin (String id, String email, String token) {

        // Forms the message content
        String bodyString = "[";
        JsonObject login_data = new JsonObject();
        login_data.addProperty(Constants.request.USER_ID, Float.valueOf(id));
        login_data.addProperty(Constants.request.USER_EMAIL, email);
        login_data.addProperty(Constants.request.USER_TOKEN, token);
        bodyString += login_data.toString();
        bodyString += "]";

        final String content = bodyString;

        // Sends the HTTPS messages to the server
        StringRequest request = new StringRequest(Request.Method.POST, Constants.url.LOGIN_RECEIVER,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> headers = new HashMap<>();

                // Sets the header parameters
                String credentials = Constants.AUTH_USER + ":" + Constants.AUTH_PASSWORD;
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);

                return headers;
            }

            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();

                return params;

            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                return content.getBytes();

            }

        };

        // Adds the request to the application requests queue
        RealHolterApplication.getInstance().getRequestQueue().add(request);

    }

}
