package com.tfg.realholter;

import android.annotation.SuppressLint;
import android.app.Application;
import android.bluetooth.BluetoothAdapter;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;

import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;


/**
 * Class that manages the general application context.
 *
 * @author Antonio Javier Nieto García
 * @version September 2017
 */
public class RealHolterApplication extends Application {

    private static RealHolterApplication sInstance;
    private static RequestQueue mRequestQueue;

    private BluetoothAdapter btadapter;
    private GoogleApiClient mGoogleApiClient;


    @Override
    public void onCreate() {

        super.onCreate();

        mRequestQueue = Volley.newRequestQueue(this);
        sInstance = this;

        // Enable bluetooth connection when starts the application
        btadapter = BluetoothAdapter.getDefaultAdapter();
        if (!btadapter.isEnabled())
            btadapter.enable();

        // Trusting all server certificates (temporal)
        handleSSLHandshake();

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }


    public synchronized static RealHolterApplication getInstance() {

        return sInstance;

    }


    public static RequestQueue getRequestQueue() {

        return mRequestQueue;

    }


    /**
     * Returns the Google API client instance.
     *
     * @return
     */
    public GoogleApiClient get_GoogleApiClient () {
        return mGoogleApiClient;
    }


    /**
     * Enables https connections
     */
    @SuppressLint("TrulyRandom")
    public static void handleSSLHandshake() {

        try {

            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {

                public X509Certificate[] getAcceptedIssuers() {

                    return new X509Certificate[0];

                }

                @Override
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }

            }};

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String arg0, SSLSession arg1) {
                    return true;
                }
            });

        } catch (Exception ignored) {

        }

    }

}
