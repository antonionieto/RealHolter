package com.tfg.realholter.activity;

import android.app.Activity;
import android.app.Application;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cortrium.opkit.ConnectionManager;
import com.cortrium.opkit.CortriumC3;
import com.cortrium.opkit.datapackages.EcgData;
import com.cortrium.opkit.datatypes.SensorMode;
import com.tfg.realholter.R;
import com.tfg.realholter.model.Extra;

import java.util.ArrayList;


/**
 * Allows the user to pair and un-pair a sensor.
 *
 * @author Antonio Javier Nieto García
 * @version Septiembre 2017
 */
public class SensorConfigActivity extends Activity implements ConnectionManager.OnConnectionManagerListener, ConnectionManager.EcgDataListener {

    // Connection parameter
    private ConnectionManager connectionManager;
    private String pairedSensor;
    private Boolean connection_status;

    // Nearby sensors list
    private ArrayList<String> names;
    private ArrayList<CortriumC3> devices;
    private ArrayAdapter<String> adapter;

    SharedPreferences preferences;

    // Layout elements
    private ListView list_view;
    private TextView sensor_name;
    private Button unpair_button;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);


        sensor_name = (TextView) findViewById(R.id.sensor_name);
        unpair_button = (Button) findViewById(R.id.exit_button);
        list_view = (ListView) findViewById(R.id.list_view);

        names  = new ArrayList<String>();
        devices = new ArrayList<CortriumC3>();

        // Prepares the sensor connection
        connectionManager = ConnectionManager.getInstance(this);
        connectionManager.setConnectionManagerListener(this);

        // Gets the saved connection parameters
        preferences = getSharedPreferences(Application.class.getName(), MODE_PRIVATE);
        pairedSensor = preferences.getString(Extra.PAIRED_SENSOR, "Null");
        connection_status = preferences.getBoolean(Extra.CONNECTION_STATUS, false);

        // Changes the state of the elementes in the layout
        if (pairedSensor != "Null"){
            sensor_name.setText(pairedSensor);
            unpair_button.setVisibility(View.VISIBLE);
        } else {
            sensor_name.setText(R.string.config_no_sensor_paired);
            unpair_button.setVisibility(View.GONE);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        // Starts scanning sensors when the application comes to the foreground
        connectionManager.startScanning();

    }


    @Override
    protected void onPause() {
        super.onPause();

        // Stops scanning sensors when the application disappears from the foreground
        connectionManager.stopScanning();

    }


    @Override
    protected void onStop() {
        super.onStop();

        // Stops scanning sensors and the connection when the application stops
        connectionManager.stopScanning();
        connectionManager.disconnect();

    }


    /**
     * Allows the user to pair and unpair a sensor.
     *
     * @param view
     */
    public void onUnpairButtonClicked(View view) {

        // If the monitor service is stopped
        if (!connection_status) {

            // Deletes the pair sensor from the system memory
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(Extra.PAIRED_SENSOR, "Null");
            editor.apply();

            // Changes the elements in the layout
            sensor_name.setText("You have not a paired sensor.");
            unpair_button.setVisibility(View.GONE);

        } else {

            // Alerts the user to stop the monitor service before un-pairing a sensor
            Toast.makeText(this, R.string.config_unpair_avoided, Toast.LENGTH_LONG).show();

        }

    }



    @Override
    public void onBackPressed() {
        finish();
    }



    @Override
    public void startedScanning(ConnectionManager connectionManager) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                // Clear the nearby sensors list when starts scanning
                names.clear();
                devices.clear();

            }
        });
    }



    @Override
    public void stoppedScanning(ConnectionManager connectionManager) {
    }



    @Override
    public void discoveredDevice(final CortriumC3 cortriumC3) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                // Saves and shows the nearby devices discovered
                if (!names.contains(cortriumC3.getName()))
                    names.add(cortriumC3.getName() + "\n");

                if (!devices.contains(cortriumC3))
                    devices.add(cortriumC3);

                showDevices(names);
            }
        });

    }


    /**
     * Shows the discovered sensors in the list. Allows the user to press in the elements of the
     * list in order to select a sensor to be paired.
     *
     * @param names Names of the discovered devices
     */
    public void showDevices(ArrayList<String> names) {

        // Prepares the list
        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, names);
        list_view.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        // Enables clicking in the elements of the list
        list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(Extra.PAIRED_SENSOR, devices.get(position).getName());
                editor.apply();

                sensor_name.setText(devices.get(position).getName());
                unpair_button.setVisibility(View.VISIBLE);
            }
        });

    }


    @Override
    public void connectedToDevice(CortriumC3 cortriumC3) {
    }


    @Override
    public void disconnectedFromDevice(CortriumC3 cortriumC3) {
    }


    @Override
    public void ecgDataUpdated(EcgData ecgData) {
    }


    @Override
    public void modeRead(SensorMode sensorMode) {
    }


    @Override
    public void deviceInformationRead(CortriumC3 cortriumC3) {
    }


}
