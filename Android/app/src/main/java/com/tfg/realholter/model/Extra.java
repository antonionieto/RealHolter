package com.tfg.realholter.model;


public class Extra {

    public static final String PAIRED_SENSOR = "Extra.PAIRED_SENSOR";
    public static final String CONNECTION_STATUS = "Extra.CONNECTION_STATUS";
    public static final String SERVER_IP = "Extra.SERVER_IP";
    public static final String USER_ID = "Extra.USER_ID";
    public static final String USER_EMAIL = "Extra.USER_EMAIL";
    public static final String USER_TOKEN = "Extra.USER_TOKEN";

}
