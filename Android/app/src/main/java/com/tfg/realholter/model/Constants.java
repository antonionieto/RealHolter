package com.tfg.realholter.model;


public class Constants {

    // HTTPS authentication credentials
    public static final String AUTH_USER = "admin";
    public static final String AUTH_PASSWORD = "admin";

    // Size of ECG array received from the sensor
    public static final int ECG_SAMPLE_SIZE = 6;

    // Low battery notification threshold
    public static final double LOW_BATTERY_THRESHOLD = 10.0;


    public static final class url {

        public static final String SERVER_URL = "https://triton.us.es:29443/endpoints/";
        public static final String HEART_RATE_RECEIVER = SERVER_URL + "ECGReceiver";
        public static final String LOGIN_RECEIVER = SERVER_URL + "LoginReceiver";
    }


    public static final class gains {
        public static final int CONF_GAIN_1 = 0x20;
        public static final int CONF_GAIN_2 = 0x40;
        public static final int CONF_GAIN_3 = 0x60;
        public static final int CONF_GAIN_4 = 0x80;
        public static final int CONF_GAIN_6 = 0x00;
        public static final int CONF_GAIN_8 = 0xA0;
        public static final int CONF_GAIN_12 = 0xC0;
    }


    public static final class request {
        public static final String USER_ID = "user_id";
        public static final String USER_EMAIL = "user_email";
        public static final String USER_TOKEN = "user_token";
        public static final String TIME = "timestamp";
        public static final String ECG_VALUE = "ecg_value";
        public static final String BODY_TEMPERATURE = "body_temperature";
        public static final String RR_INTERVAL = "rr_interval";
        public static final String RESPIRATORY_RATE = "respiratory_rate";
        public static final String HEAR_RATE = "heart_rate";

    }

}
