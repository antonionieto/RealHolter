package com.tfg.realholter.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.tfg.realholter.R;
import com.tfg.realholter.RealHolterApplication;
import com.tfg.realholter.RealHolterService;
import com.tfg.realholter.model.Extra;


/**
 * Manages the main page where the user may enable/disable
 * the monitor service and access to the settings page. Also,
 * it allows logging out.
 *
 * @author Antonio Javier Nieto García
 * @version September 2017
 */
public class MainActivity extends Activity {


    private GoogleApiClient mGoogleApiClient;
    private SharedPreferences preferences;

    // Parameters used in the sensor connection management
    private String paired_sensor;
    private Boolean connection_status;

    // Sensor connection parameters
    private TextView m_name;
    private TextView m_status;

    // Layout button elements
    private Button button;
    private Button sensor_config_button;
    private Button logout_button;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        button = (Button) findViewById(R.id.button);
        m_name = (TextView) findViewById(R.id.name);
        m_status = (TextView) findViewById(R.id.status);
        logout_button = (Button) findViewById(R.id.logout_button);
        sensor_config_button = (Button) findViewById(R.id.sensor_config_button);

        preferences = getSharedPreferences(Application.class.getName(), MODE_PRIVATE);

        // Gets GoogleApiClient instance and connect to Google Play services
        mGoogleApiClient = RealHolterApplication.getInstance().get_GoogleApiClient();
        mGoogleApiClient.connect();


        // Sets the button click listeners
        sensor_config_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Class<? extends Activity> activity = SensorConfigActivity.class;
                Intent intent = new Intent(MainActivity.this, activity);
                startActivity(intent);
            }
        });
        logout_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });

    }


    @Override
    protected void onResume() {

        super.onResume();

        // Gets the sensor connection parameters
        // Sensor paired name
        paired_sensor = preferences.getString(Extra.PAIRED_SENSOR, "Null");
        // Current connection status
        connection_status = preferences.getBoolean(Extra.CONNECTION_STATUS, false);

        // Changes the appearance of the layout elements according to the status of the connection
        if (connection_status){
            button.setText(R.string.main_stop_button);
            m_status.setText(R.string.main_status_connected);
        } else {
            button.setText(R.string.main_start_button);
            m_status.setText(R.string.main_status_disconnected);
        }

        if (paired_sensor != "Null"){
            m_name.setText(paired_sensor);
        } else {
            m_name.setText(R.string.main_unpaired_sensor);
        }

    }

    /**
     * Manages the logging out.
     */
    private void logout () {

        // Deletes the user information stored in the system memory
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Extra.USER_ID, "null");
        editor.putString(Extra.USER_EMAIL, "null");
        editor.putString(Extra.USER_TOKEN, "null");
        editor.apply();

        // Google signing out
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        finish();
                    }
                });

    }


    /**
     * Allows the user to enable and disable the monitor service.
     *
     * @param view
     */
    public void onStartStopButtonClicked(View view) {

        SharedPreferences.Editor editor = preferences.edit();


        // If the sensor connection was disabled
        if (!connection_status){

            // There is a paired sensor
            if (paired_sensor != "Null") {

                // Starts the monitor service
                startService(new Intent(MainActivity.this, RealHolterService.class));

                // Saves the current status of the connection
                connection_status = true;
                editor.putBoolean(Extra.CONNECTION_STATUS, true);

                // Changes the state of the elements in the layout
                button.setText(R.string.main_stop_button);
                m_status.setText(R.string.main_status_connected);

            } else {

                // Alerts the user to pair a sensor before starting the monitor service
                AlertDialog.Builder btdialog = new AlertDialog.Builder(this);
                btdialog.setTitle("Sensor paired");
                btdialog.setMessage("Please, pair a sensor before starting to monitor your health!");
                btdialog.setCancelable(false);
                btdialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface btdialog, int id) {
                    }
                });
                btdialog.show();

            }

        } else {

            // Stops the monitor service
            stopService(new Intent(MainActivity.this, RealHolterService.class));

            // Saves the current status of the connection
            connection_status = false;
            editor.putBoolean(Extra.CONNECTION_STATUS, false);

            // Changes the state of the elements in the layout
            button.setText(R.string.main_start_button);
            m_status.setText(R.string.main_status_disconnected);

        }

        editor.apply();

    }


}
