package com.tfg.realholter.activity;

import android.app.Activity;
import android.os.Bundle;
import com.tfg.realholter.R;


/**
 * Show the application info on the screen.
 *
 * @author Antonio Javier Nieto García
 * @version September 2017
 */
public class AboutActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

    }

}
