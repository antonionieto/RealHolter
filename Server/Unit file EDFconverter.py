[Unit]
Description=Gunicorn instance to serve EDFconverter
After=network.target

[Service]
User=root
Group=root
WorkingDirectory=/home/flask
Environment="PATH=/home/flask/bin"
ExecStart=/home/flask/bin/gunicorn EDFconverter:app -b 127.0.0.1:62222

[Install]
WantedBy=multi-user.target