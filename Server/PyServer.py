#!flask/bin/python

from flask import Flask, jsonify, request, abort
from flask_httpauth import HTTPBasicAuth
import json, requests, sys, commands, os, datetime, numpy as np, pyedflib, time
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from pyfcm import FCMNotification
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
import time


requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
auth = HTTPBasicAuth()


# FCM parameters - Server key
push_service = FCMNotification(api_key="AAAAHbHzxcI:APA91bHYnRZktsH-DwiuwSum3dQoYCCpwG-jhwYOwlyqboccFN2K7quNnGyoydN7kMD53XmIRNLVwNQVoU7PWtGZNGe54QA6Zu3P4Id8ZZhFgnLjbThLj_cVKqj8382FFd2_ZXKbaTvS")

app = Flask(__name__)



@auth.get_password
def get_password(username):
        if username == 'daspy':
                return 'pass4daspy'
        return None


@auth.error_handler
def unauthorized():
        return make_response(jsonify({'error': 'Unauthorized access'}), 401)



def create_edf (ecg_list, body_temp_list, rr_interval_list, respiration_rate_list, heart_rate_list):
    
    'Stores the sensor data into a EDF+ format file.'

    # Preparing file
    filename = "EDF_"+time.strftime("%Y%m%d_%H%M%S")+".edf"
    test_data_file = os.path.join('/home/EDF/files', filename)
    f = pyedflib.EdfWriter(test_data_file, 5,
                           file_type=pyedflib.FILETYPE_EDFPLUS)
    

    # Data arrays
    channel_info = []
    data_list = []

    ch_dict = {'label': 'ECG', 'dimension': 'uV', 'sample_rate': 250, 'physical_max': 600, 'physical_min': -600, 'digital_max': 32767, 'digital_min': -32768, 'transducer': '', 'prefilter':''}
    channel_info.append(ch_dict)
    r2 = np.array(ecg_list)
    data_list.append(r2)

    ch_dict = {'label': 'Temp body', 'dimension': 'degC', 'sample_rate': 41.66667, 'physical_max': 45, 'physical_min': 32, 'digital_max': 32767, 'digital_min': -32768, 'transducer': '', 'prefilter':''}
    channel_info.append(ch_dict)
    r2 = np.array(body_temp_list)
    data_list.append(r2)

    ch_dict = {'label': 'Dur RR', 'dimension': 'ms', 'sample_rate': 41.66667, 'physical_max': 600, 'physical_min': 0, 'digital_max': 32767, 'digital_min': -32768, 'transducer': '', 'prefilter':''}
    channel_info.append(ch_dict)
    r2 = np.array(rr_interval_list)
    data_list.append(r2)

    ch_dict = {'label': 'Resp body', 'dimension': '', 'sample_rate': 41.66667, 'physical_max': 300, 'physical_min': 0, 'digital_max': 32767, 'digital_min': -32768, 'transducer': '', 'prefilter':''}
    channel_info.append(ch_dict)
    r2 = np.array(respiration_rate_list)
    data_list.append(r2)

    ch_dict = {'label': 'Vel heart', 'dimension': 'bpm', 'sample_rate': 41.66667, 'physical_max': 240, 'physical_min': 0, 'digital_max': 32767, 'digital_min': -32768, 'transducer': '', 'prefilter':''}
    channel_info.append(ch_dict)
    r2 = np.array(heart_rate_list)
    data_list.append(r2)
    

    # Writing file
    f.setSignalHeaders(channel_info)
    f.writeSamples(data_list)
    f.close()
    del f


    return None




def purge_data(initial_date, final_date):

    'Executes a script to purge the data stored in the DAS database.'

    # Preparing command string
    command_string = '/home/wso2das-3.1.0/bin/analytics-backup.sh -purge -table "ECGSTREAM" -tenantId -1234 -timeFrom ' + initial_date + ' -timeTo ' + final_date
    result = os.system(command_string)

    return result




@app.route('/api/fever_alert', methods=['POST'])
@auth.login_required
def fever_alert():

    '''
    Alerts the user when the server detects fever in the 
    user temperature.

    The server sends a HTTPS POST message to the URL
    http://127.0.0.1:62222/api/fever_alert that contains
    the information necessary to fetch the data from the
    server database.

    Notification ways:

        -   Firebase Cloud Messaging (FCM): sends a push
            notification to the user device.

        -   E-mail: sends a email to the user mail account
            that the user used when logging in the system.

    '''

    # Check if the content format is JSON
    if not request.json:    
        abort(400)

    # Parses the message content
    json_parsed = json.loads(request.data)
    content = json_parsed["event"]["payloadData"]

    # Gets the user_ID and mean temperature from the mesage content
    user_id = str(content['user_id'])
    temperature = str(content['avgTemp'])

    # Requests the user's email and token to the server, using the user's ID
    payload = {
	    "tableName":"LOGINSTREAM",
            "query":"user_id:" + user_id,
	    "start":0,
	    "count":1
	}

    url = "https://localhost:9443/analytics/search"
    headers = {'Content-Type' : 'application/json'}
    response = requests.post(url, data=json.dumps(payload), headers=headers, verify=False, auth=('admin', 'admin'))

    # Get the requested data from the answer message
    user_email = json.loads(response.text)[0]["values"]["user_email"]
    user_token = json.loads(response.text)[0]["values"]["user_token"]
        
    
    # Prepare the FCM notification
    registration_id = user_token
    message = "RealHolter has recently detected your body temperature has increased until " + temperature + " grades centigrades. "
    message += "We strongly recommend you to go to the nearest hospital as soon as possible!"
    message_title = "RealHolter fever Alert"
        
    # Send push notification to the user device
    result = push_service.notify_single_device(registration_id=registration_id, message_title=message_title, message_body=message)


    # Prepating the email notification
    fromaddr = "realholternotifications@gmail.com"
    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = user_email
    msg['Subject'] = "RealHolter fever alert"
    body = "<h1>REALHOLTER ALERT</h1><p>Recently, your RealHolter application has detected your body temperature<strong> is over 38ºC</strong>. We strongly recommend you to go to the nearest hospital, you may have <strong>fever.</strong></p>"
    msg.attach(MIMEText(body, 'html'))
          
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login(fromaddr, "pass4daspy")
    text = msg.as_string()

    # Send email
    server.sendmail(fromaddr, user_email, text)
    server.quit()



@app.route('/api/edf', methods=['POST'])
@auth.login_required
def receive_edf():

    '''
    Fetch and prepares the sensor data to be store in a 
    EDF+ format file. 

    The server sends a HTTPS POST message to the URL
    http://127.0.0.1:62222/api/edf that contains the 
    information related to the first and the last event
    in the recording interval. Then, the script request
    the data within the interval and call a function to
    convert the data to EDF+.
    '''

    # Check if the format of the message is JSON
    if not request.json:
        abort(400)

    # Parses the data from the content
    json_parsed = json.loads(request.data)
    content = json_parsed["event"]["payloadData"]


    # Getting initial timestamp
    payload = {
	"tableName":"ECGSTREAM",
        "query":"timestamp:"+"{:.0f}".format(content['startTime']),
	"start":0,
	"count":1
    }
    url = "https://localhost:9443/analytics/search"
    headers = {'Content-Type' : 'application/json'}

    response = requests.post(url, data=json.dumps(payload), headers=headers, verify=False, auth=('admin', 'admin'))
    initial_ts = json.loads(response.text)[0]["timestamp"]


    # Getting final timestamp
    payload = {
        "tableName":"ECGSTREAM",
        "query":"timestamp:"+"{:.0f}".format(content['endTime']),
        "start":0,
        "count":1
    }
    url = "https://localhost:9443/analytics/search"
    headers = {'Content-Type' : 'application/json'}

    # Wait for 3 seconds before requesting the data
    time.sleep(3)

    response = requests.post(url, data=json.dumps(payload), headers=headers, verify=False, auth=('admin', 'admin'))
    final_ts = json.loads(response.text)[0]["timestamp"]
	

    # Fetching data from the record time interval
    url = "https://localhost:9443/analytics/tables/ECGSTREAM/"+str(initial_ts)+"/"+str(final_ts+1)
    response = requests.get(url, auth=('admin', 'admin'), verify=False)
	
    # Extracting the data from the response    
    content = json.loads(response.text)
	
    ecg_list = []
    body_temp_list = []
    rr_interval_list = []
    respiration_rate_list = []
    heart_rate_list = []

    # Saving data into lists
    for event in content:
	ecg_list.append(event["values"]["ecg_value"])
	body_temp_list.append(event["values"]["body_temperature"])
	rr_interval_list.append(event["values"]["rr_interval"])
    	respiration_rate_list.append(event["values"]["rr_interval"])
	heart_rate_list.append(event["values"]["heart_rate"])

    # Creating EDF file
    create_edf(ecg_list, body_temp_list, rr_interval_list, respiration_rate_list, heart_rate_list)

    # Preparing time interval to purge the data
    initial_date = datetime.datetime.fromtimestamp(initial_ts/1000)
    final_date = datetime.datetime.fromtimestamp(final_ts/1000)

    initial_date_formated = initial_date.strftime("%y-%m-%d-%H:%M:%S")
    final_date_formated = final_date.strftime("%y-%m-%d-%H:%M:%S")

    # Purge the data
    command_result = purge_data(initial_date_formated, final_date_formated)

    return jsonify(payload)




if __name__ == '__main__':
	
    if len(sys.argv) == 1:
        # Listening the local port 62222
        app.run(host='127.0.0.1', port=int("62222"), debug=True)
    else:
        print("Usage python app.py")

